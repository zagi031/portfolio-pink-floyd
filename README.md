# Pink Floyd portfolio project

Pink Floyd portfolio is an infographic project to show the basic facts about famous band Pink Floyd. You can see the members and the story of mentioned band. Also, there is a visualized wall with all of their albums and at the bottom of the page you can find table with all of their tours.
            
To visit the site, click [here](https://pink-floyd-portfolio.herokuapp.com/).

Technologies used: HTML and CSS
